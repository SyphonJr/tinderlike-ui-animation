import 'dart:math';

import 'package:flutter/material.dart';

/// Springy curve, from here https://blog.funwith.app/posts/custom-curves-in-flutter/
class SpringCurve extends Curve {
  const SpringCurve({
    this.a = 0.15,
    this.w = 19.4,
  });
  final double a;
  final double w;

  @override
  double transformInternal(double t) {
    return -(pow(e, -t / a) * cos(t * w)) + 1;
  }
}

enum SwipeDirection { left, right }

class CarouselController {
  Function(SwipeDirection) dismiss;
}

/// Carousel that slides cards from a stack, back to front.
///
/// The first card can be removed by using the [controller.dismiss].
/// This widget assumes the whole available space is used.
class Carousel extends StatefulWidget {
  // List of widgets used
  final List<Widget> listCards;

  final CarouselController carouselController;

  // Resizing of the card widgets multiplier. A sizeMultiplier of 0.1 means the biggest card (the one in the front),
  // will be of 90% size of the total available space. That 10% extra space is used to space evenly two other cards.
  final double sizeMultiplier;
  Carousel(this.listCards, this.carouselController,
      [this.sizeMultiplier = 0.1]);

  @override
  _CarouselState createState() => _CarouselState();
}

class _CarouselState extends State<Carousel> with TickerProviderStateMixin {
  AnimationController _controller;
  List<Widget> _listCards = [];
  Animation<double> _rotateTween;
  Animation<double> _horizontalTween;

  @override
  void initState() {
    widget.carouselController.dismiss = dismiss;
    _listCards = widget.listCards;
    _controller = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1000));

    _controller.addListener(() {
      setState(() {});
    });

    _controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        setState(() {
          _listCards.removeAt(0);
          _controller.stop();
          _controller.reset();
        });
      }
    });

    _rotateTween = Tween<double>(
      begin: 0,
      end: pi / 2,
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Interval(0, 0.800, curve: Curves.ease),
      ),
    );

    _horizontalTween = Tween<double>(begin: 0, end: 1000).animate(
        CurvedAnimation(
            curve: Interval(0, 0.800, curve: Curves.ease),
            parent: _controller));

    super.initState();
  }

  void dismiss(SwipeDirection direction) {
    if (_controller.isAnimating) {
      setState(() {
        _listCards.removeAt(0);
        _controller.stop();
        _controller.reset();
      });
    }

    if (direction == SwipeDirection.left) {
      _horizontalTween = Tween<double>(begin: 0, end: -1000).animate(
          CurvedAnimation(
              curve: Interval(0, 0.800, curve: Curves.ease),
              parent: _controller));

      _rotateTween = Tween<double>(
        begin: 0,
        end: -(pi / 2),
      ).animate(
        CurvedAnimation(
          parent: _controller,
          curve: Interval(0, 0.800, curve: Curves.ease),
        ),
      );
    }

    if (direction == SwipeDirection.right) {
      _horizontalTween = Tween<double>(begin: 0, end: 1000).animate(
          CurvedAnimation(
              curve: Interval(0, 0.800, curve: Curves.ease),
              parent: _controller));

      _rotateTween = Tween<double>(
        begin: 0,
        end: pi / 2,
      ).animate(
        CurvedAnimation(
          parent: _controller,
          curve: Interval(0, 0.800, curve: Curves.ease),
        ),
      );
    }

    _controller.forward();
  }

  /// Constructs the cards in the stack
  List<Widget> setupCards(BoxConstraints constraints) {
    List<Widget> listWidget = [];
    final sizeMultiplier = widget.sizeMultiplier;

    // Not the most sophisticated code, but gets the job done.
    // If there's cards left in the list, we put it in the listWidget.

    // First card in the stack. This card gets pushed to the bottom depending on how much smaller it gets because of the [sizeMultiplier]
    // During the animation, this card will be moved away sidewards.

// Transform.translate(
//         offset: Tween<Offset>(begin: Offset.zero, end: Offset(0, -300))
//             .animate(_controller)
//             .value,
//         child: Transform.translate(
//           offset: Offset(_horizontalTween.value, 0),
//           child:

    if (_listCards.isNotEmpty) {
      listWidget.add(
        Transform.translate(
          offset: Tween<Offset>(begin: Offset.zero, end: Offset(0, -500))
              .animate(CurvedAnimation(
                  curve: Interval(0, 0.800, curve: Curves.ease),
                  parent: _controller))
              .value,
          child: Transform.translate(
            offset: Offset(_horizontalTween.value, 0),
            child: Transform.rotate(
              angle: _rotateTween.value,
              child: Transform.translate(
                offset: Offset(0, constraints.maxHeight * sizeMultiplier),
                child: Container(
                  child: _listCards[0],
                  height: constraints.maxHeight * (1 - sizeMultiplier),
                  width: constraints.maxWidth,
                ),
              ),
            ),
          ),
        ),
      );
    }

    // Second card in the stack. This card will be between the first and the third card. This card also gets pushed down depending on the [sizeMultiplier] / 2
    // During the animation this card will take the place of the first card and have a springeffect, making it look like it got pushed hard to his new place.
    if (_listCards.length >= 2) {
      Widget secondCard = _listCards[1];

      // Wrap it in a container, setting its size depending on the sizeMultiplier
      // If the animation is running, the size will be the same as the first card.
      secondCard = Container(
          child: secondCard,
          height: _controller.status == AnimationStatus.forward ||
                  AnimationStatus.completed == _controller.status
              ? constraints.maxHeight * (1 - sizeMultiplier)
              : constraints.maxHeight * (1 - (sizeMultiplier * 2)),
          width: _controller.status == AnimationStatus.forward ||
                  AnimationStatus.completed == _controller.status
              ? constraints.maxWidth
              : constraints.maxWidth * (1 - sizeMultiplier));

      // Translate the card
      secondCard = Transform.translate(
        offset: _controller.status == AnimationStatus.forward ||
                AnimationStatus.completed == _controller.status
            ? Offset(0, constraints.maxHeight * sizeMultiplier)
            : Offset(0, (constraints.maxHeight * sizeMultiplier) / 2),
        child: secondCard,
      );

      // During animation, the card will get a springy effect by scaling it.
      secondCard = Transform.scale(
          scale: _controller.status == AnimationStatus.forward ||
                  AnimationStatus.completed == _controller.status
              ? Tween<double>(begin: 0.85, end: 1)
                  .animate(CurvedAnimation(
                      parent: _controller, curve: SpringCurve()))
                  .value
              : 1,
          child: secondCard);

      listWidget.add(secondCard);
    }

    // Third card. During animation this card will move to the second cards position.
    if (_listCards.length >= 3) {
      listWidget.add(
        Transform.translate(
          offset: Tween<Offset>(
            begin: Offset.zero,
            end: Offset(0, (constraints.maxHeight * sizeMultiplier) / 2),
          )
              .animate(
                CurvedAnimation(
                    parent: _controller, curve: Curves.easeOutCubic),
              )
              .value,
          child: Container(
            child: _listCards[2],
            height: constraints.maxHeight * (1 - (sizeMultiplier * 3)),
            width: Tween<double>(
                    begin: constraints.maxWidth * (1 - (sizeMultiplier * 2)),
                    end: constraints.maxWidth * (1 - sizeMultiplier))
                .animate(CurvedAnimation(
                    parent: _controller, curve: Curves.easeOutCubic))
                .value,
          ),
        ),
      );
    }

    // This is the last card in that's 'shown' on screen. It will be hidden behind the third card. When the third card
    // animates to the front, this card will show behind it, creating an effect that a new card gets on the stack.
    if (_listCards.length >= 4) {
      listWidget.add(
        Container(
          child: _listCards[3],
          height: constraints.maxHeight * (1 - (sizeMultiplier * 3)),
          width: constraints.maxWidth * (1 - (sizeMultiplier * 2)),
        ),
      );
    }
    listWidget = listWidget.reversed.toList();

    return listWidget;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 50, 20, 30),
      child: Column(
        children: [
          Expanded(
            child: LayoutBuilder(
              builder: (context, constraints) {
                return Stack(
                  alignment: Alignment.topCenter,
                  children: setupCards(constraints),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _controller.dispose();
    super.dispose();
  }
}
