import 'package:flutter/material.dart';
import 'package:flutter_tinder_ui/carousel.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Main(),
    );
  }
}

class Main extends StatefulWidget {
  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {
  CarouselController controller;

  @override
  void initState() {
    controller = CarouselController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF6555A4),
      body: SizedBox.expand(
        child: Column(
          children: [
            Expanded(
              child: Carousel([
                TempCard(
                  color: Colors.red,
                  id: 1,
                  image: 'images/1.jpg',
                  firstButtonHandler: () {
                    controller.dismiss(SwipeDirection.left);
                  },
                  secondButtonHandler: () {
                    controller.dismiss(SwipeDirection.right);
                  },
                ),
                TempCard(
                  color: Colors.blue,
                  id: 2,
                  image: 'images/2.jpg',
                  firstButtonHandler: () {
                    controller.dismiss(SwipeDirection.left);
                  },
                  secondButtonHandler: () {
                    controller.dismiss(SwipeDirection.right);
                  },
                ),
                TempCard(
                  color: Colors.green,
                  id: 3,
                  image: 'images/3.jpg',
                  firstButtonHandler: () {
                    controller.dismiss(SwipeDirection.left);
                  },
                  secondButtonHandler: () {
                    controller.dismiss(SwipeDirection.right);
                  },
                ),
                TempCard(
                  color: Colors.red,
                  id: 4,
                  image: 'images/4.jpg',
                  firstButtonHandler: () {
                    controller.dismiss(SwipeDirection.left);
                  },
                  secondButtonHandler: () {
                    controller.dismiss(SwipeDirection.right);
                  },
                ),
              ], controller, 0.07),
            ),
            Container(
              height: 60,
              color: Color(0xFF776BB6),
              child: SizedBox.expand(child: Center(child: Text('Appbar'))),
            ),
          ],
        ),
      ),
    );
  }
}

class TempCard extends StatelessWidget {
  final Color color;
  final int id;
  final String image;
  final VoidCallback firstButtonHandler;
  final VoidCallback secondButtonHandler;

  const TempCard(
      {Key key,
      this.color,
      this.id,
      this.image,
      this.firstButtonHandler,
      this.secondButtonHandler})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print('tap');
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) => DetailCard(
              id: id,
              image: image,
            ),
          ),
        );
      },
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
        child: Column(
          children: [
            Expanded(
              flex: 4,
              child: Hero(
                tag: '$id picture',
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Image.asset(image, fit: BoxFit.fill)),
              ),
            ),
            Hero(
              tag: '$id buttons',
              child: Material(
                child: Container(
                  height: 100,
                  color: Color(0xFF776BB6),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                        onTap: () {
                          firstButtonHandler();
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Color(0xFFFE6666),
                              borderRadius: BorderRadius.circular(50)),
                          width: 100,
                          height: 70,
                          child: Center(
                            child: Text(
                              'Don\'t',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          secondButtonHandler();
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Color(0xFF00FFFF),
                              borderRadius: BorderRadius.circular(50)),
                          width: 100,
                          height: 70,
                          child: Center(
                            child: Text(
                              'I\'m in',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DetailCard extends StatelessWidget {
  final int id;
  final String image;
  const DetailCard({
    Key key,
    this.id,
    this.image,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Hero(
              tag: '$id picture',
              child: Container(child: Image.asset(image, fit: BoxFit.fill)),
            ),
          ),
          Expanded(
            child: Container(),
          ),
          Hero(
            tag: '$id buttons',
            child: Material(
              child: Container(
                height: 100,
                color: Color(0xFF776BB6),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color(0xFFFE6666),
                            borderRadius: BorderRadius.circular(50)),
                        width: 100,
                        height: 70,
                        child: Center(
                          child: Text(
                            'Don\'t',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color(0xFF00FFFF),
                            borderRadius: BorderRadius.circular(50)),
                        width: 100,
                        height: 70,
                        child: Center(
                          child: Text(
                            'I\'m in',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CarouselCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
